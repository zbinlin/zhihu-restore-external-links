// ==UserScript==
// @id             restore-external-links@zhihu
// @name           restore external links for zhihu
// @version        1.2
// @namespace      @zhihu
// @author         zbinlin
// @description    还原 zhihu 问答里的外链
// @updateURL      https://bitbucket.org/zbinlin/zhihu-restore-external-links/raw/tip/zhihu-restore-external-links.meta.js
// @include        http://www.zhihu.com/question/*
// @include        https://www.zhihu.com/question/*
// @include        http://zhuanlan.zhihu.com/*
// @include        https://zhuanlan.zhihu.com/*
// @run-at         document-end
// ==/UserScript==
