// ==UserScript==
// @id             restore-external-links@zhihu
// @name           restore external links for zhihu
// @version        1.2
// @namespace      @zhihu
// @author         zbinlin
// @description    还原 zhihu 问答里的外链
// @updateURL      https://bitbucket.org/zbinlin/zhihu-restore-external-links/raw/tip/zhihu-restore-external-links.meta.js
// @include        http://www.zhihu.com/question/*
// @include        https://www.zhihu.com/question/*
// @include        http://zhuanlan.zhihu.com/*
// @include        https://zhuanlan.zhihu.com/*
// @run-at         document-end
// ==/UserScript==

"use strict";

let observer = new MutationObserver(debounce(function __mutationCallback__() {
    restoreExternalLinks(document.links);
}, 500));

observer.observe(document, {
    childList: true,
    subtree: true,
});

function restoreExternalLinks(links) {
    links = Array.prototype.slice.call(links);
    for (let i = 0, len = links.length; i < len; i++) {
        let link = links[i];
        if (/^https?:\/\/link\.zhihu\.com\/\?target=([^$]+)$/.test(link.href)) {
            let href = RegExp.$1;
            try {
                href = decodeURIComponent(href);
            } catch (ex) {
                // empty
            }
            link.href = href;
        }
    }
}

restoreExternalLinks(document.links);

/*
 * @param {Function} func
 *   需要进行去抖的函数
 * @param {number} wait
 *   去抖的时间间隔
 * @param {boolean} [lead = false]
 *   是否要在开始执行
 * @returns {Function}
 *   返回一个去抖函数
 */
function debounce(func, wait, lead) {
    var tid, timestamp,
        that, args;

    function delayed(timeout) {
        tid && clearTimeout(tid);
        tid = setTimeout(function () {
            handler();
        }, timeout);
    }

    function now() {
        return Date.now ? Date.now() : new Date().getTime();
    }

    function handler() {
        return func.apply(that, args);
    }

    return function __debounced__() {
        args = arguments;
        that = this;

        if (lead) {
            if (!timestamp || (now() - timestamp) >= wait) {
                handler();
            }
            timestamp = now();
        } else {
            delayed(wait);
        }
    };
}
